import argparse

from random import shuffle
from math import floor


parser = argparse.ArgumentParser()
parser.add_argument('-n', type=int, choices=range(2, 101), default=10,
                    help='An amount of servers to count data loss possibility')
parser.add_argument('--mirror', choices=['mirror', 'random'], default='mirror',
                    help='Choose between "mirror" and "random" mode')


SHARDS_N = 100

# Distribute specified amount(=SHARDS_N) of shards on n/2 servers
def count_shards(n):
    if n%2 == 0:
        servers_amount = int(n/2)
    else:
        servers_amount = int((n-1)/2)
    shards_amount = floor(SHARDS_N/servers_amount)
    return shards_amount, servers_amount

# Generate list of shards on n/2 servers
def generate_shards():
    shards = [x for x in range(1, (101-(SHARDS_N-servers_amount*shards_amount)))]
    shuffle(shards)
    return shards

# Split list of shards into a list of servers
def split(shards):
    servers = []
    while len(shards) > shards_amount:
        server = shards[:shards_amount]
        servers.append(server)
        shards = shards[shards_amount:]
    servers.append(shards)
    return servers

# Generate servers depending on mode: 'random' or 'mirror'
def generate_mirror_servers(n):
    servers = split(shards)
    mirror_servers = servers + servers
    return mirror_servers

def generate_random_servers(n):
    replicas = shards[:]
    shuffle(replicas)
    random_servers = split(shards + replicas)
    return random_servers

def check_probability():
    # Making list of sets(=servers) from the list of lists
    servers_set = [set(server) for server in servers]
    # [print(idx, server) for idx, server in enumerate(servers_set)]

    # Checking if servers have intersected shards. Appending results of
    #   comparing to a list
    probabilities = []
    for server in range(len(servers_set)):
        for idx in range(len(servers_set)):
            if server != idx:
                intersected = servers_set[server].intersection(servers_set[idx])
                if len(intersected) != 0:
                    probabilities.append(1)
                    intersected = []
                else:
                    probabilities.append(0)

    # Finally calculating % of data loss cases
    probability = round(sum(probabilities)/len(probabilities) * 100, 1)
    return probability

def print_result():
    return (print("Killing 2 arbitrary servers results in data loss in %s%% cases" % probability))


if __name__ == '__main__':
    args = parser.parse_args()
    print(args.n, args.mirror)

    def choose_mode(mode):
        if mode == 'mirror':
            servers = generate_mirror_servers(servers_shards_amount)
            return servers
        elif mode == 'random':
            servers = generate_random_servers(servers_shards_amount)
            return servers

    servers_shards_amount = (shards_amount, servers_amount) = count_shards(args.n)
    shards = generate_shards()
    servers = choose_mode(args.mirror)
    probability = check_probability()
    print_result()
